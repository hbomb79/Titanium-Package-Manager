### Installing
See the [Titanium Download Guide](http://harryfelton.web44.net/titanium/guide/download) for information on how to download and install the Titanium Developer Tools.

### Further Help
Click the links below for web-based help for programs bundled with TDT (each program is bundled with ingame help too):
- [Titanium Package Manager (tpm)](./help/TPM.md)
- [Titanium Developer Tools (tdt)](./help/TDT.md)
- [Titanium Packager](https://gitlab.com/hbomb79/Titanium/blob/develop/bin/package.lua)
