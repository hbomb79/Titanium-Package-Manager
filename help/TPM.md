Titanium Package Manager
===

The Titanium package manager provides a quick and easy to use interface for downloading Titanium and programs developed with it. This centralised system allows systems to manage the installations of packages easily.

### Usage
Running `tpm help` will show the modes and flags you can use. This section is more focused on providing examples of common configurations

##### Fetching tags
The TPM doesn't query the web server for tags every time a command is run. Instead, `tpm fetch` is used to save the tags to file at `/.tpm/cache`, this cache is then used by the program whenever it needs to know what tags are available.

##### Package Versions
If you need to know what versions of a project are available to download, use `tpm versions [package, ...]`

Running `tpm versions` will show the versions for all available projects, whereas `tpm versions Titanium` will only show the versions of Titanium available to download.

##### Listing Packages
Run `tpm list` to view all the packages installed on the system, along with the versions of those packages installed

##### Installing Packages
Installing a package is easy. If you need a specific version of a package, specify it using a `:`.

For example, install the latest version of Titanium:  
`tpm install Titanium` _or_ `tpm install Titanium:latest`

Install a specific version:  
`tpm install Titanium:v0.1.0-alpha.5`

Packages are installed to `/.tpm/packages/:name/:version`. If you want to access a package without using that path, the `--dest` flag can be used.

When a destination is set using the `--dest` flag, a shortcut will be created at that location pointing to the actual package. When the package is uninstalled, the shortcut is deleted. A package can have multiple shortcuts.

Note: Be sure not to edit a shortcut file, as any edits will not be preserved and will be removed when the package is uninstalled.

##### Package Versions
Titanium package manager allows you to install multiple versions of the same package, this features comes with additional complexity - although it is still very simple.

A package can be permanent, or disposable. This is dictated by the `--disposable` flag. If a package is disposable, it will be automatically removed when all of it's depending files are missing.

For example, `tpm install Titanium --disposable --depend startup` tells TPM that the startup file depends on the latest version of Titanium. Because the installation is disposable, it means that if the startup file is removed, the latest version of Titanium is automatically removed.

Likewise, if a new Titanium release is pushed, TPM will automatically update that dependency to the new version. If no other dependencies exist for the old version, it will be removed. This means no versions of a package are installed unless they are depended on.

If a package is not installed as disposable, it will not be automatically removed and must be manually uninstalled (see below).

##### Uninstalling Packages
To uninstall all packages, `tpm uninstall` can be used. This will uninstall all versions of every package

`tpm uninstall package package2` would uninstall the latest version of `package` and `package2`. To uninstall a specific version of a package, the `:` syntax can be used - much like when installing.

For example, uninstall all versions of Titanium  
`tpm uninstall Titanium:*`

Uninstall the latest version of Titanium  
`tpm uninstall Titanium` _or_ `tpm uninstall Titanium:latest`

Uninstall a specific version of Titanium  
`tpm uninstall Titanium:v0.1.0-alpha.5`