Titanium Developer Tools
===

The Titanium Developer Tools (TDT) make developing applications for Titanium much easier. It allows for easy project switching, configuration, building and running.

*TDT must be installed before use. Ensure you have all dependencies installed by using the [installer](./README.md#installing)*

### Usage
Running `tdt help` will show information about each command. Use `tdt help <command>` to see help information regarding a specific command.

Use `tdt help settings` to see information regarding available settings for a TDT projects (these settings can be changed using [`tdt set`](#Setting Properties)). Use `tdt help settings <setting>` to see information regarding a specific setting.

#### Creating Projects
Before you can get developing, you'll need to create a new project. A project is basically a container for all your build settings.

Use `tdt new <name>` will create a project with the name you provide. This project can then be opened (using `tdt open <name>`), although TDT will automatically open the new project for you if you hit 'Y' after creating a new project.

Use `tdt close` to close the currently open project.

#### Configuring Projects
Projects can be configured using a variety of commands. Settings such as source file paths, file exclusions, build settings, output settings, and more need to be handled by TDT so that your project builds correctly.

##### Source
Of course, your project *needs* to know where your source files are. There are three types of source files:
- **source** Source files are stored inside the projects virtual file system. They are not saved on the client when the build is executed (your end-user won't see them), but your project can access them as if they were.
- **class source** Class files are inaccessible to your projects source files. Once Titanium loads the classes inside of them, they are 'discarded'. This means that every file inside of your class sources *must* create a Titanium class.
- **extract** Extract files are saved on the end-users computer when the build is executed (traditional packager). Files are overwritten if need be.

To add files to your project, three commands can be used to add the different source types: `tdt add <path>`, `tdt add-class <path>` and `tdt add-extract <path>`. For each of these commands there is a remover (eg: `add` has `remove`, and `add-extract` and `remove-extract`. These functions accept the same arguments, and simply remove the path from the projects source files).

###### Exclusions
Paths can be excluded from your projects source files using the following commands:

- **tdt exclude**: Excludes the path provided from ANY processing
- **tdt exclude-class**: Excludes the path provided from being used as a class source
- **tdt exclude-vfs**: Excludes the path provided from being available via the packages virtual file system
- **tdt exclude-extract**: Excludes the path provided from being extracted
- **tdt exclude-pattern**: Any path that matches the Lua pattern provided, will be excluded from any processing. More than one Lua pattern can be used at a time.

For every command listed above, there is an 'include' variant (ie: `tdt exclude-vfs` can be reversed using `tdt include-vfs`).

###### Initialisation Files
When your build file is executed, a file (init file) must be selected to run straight away (think of this like your 'startup' file). Without one the project will simply close instantly. There are multiple types of init files, listed below in the order they are executed:

- **pre-init** This file is run before anything really happens. Titanium has not been loaded yet so you can use this file to load your own version of Titanium (if Titanium is not loaded after this file finishes execution then the package will load it's own version (or crash if no version set))
- **titanium-init** This file is run just after Titanium is loaded, and just before your projects classes are loaded ('class source' files). This gives the oppourtunity to load scripts that are dependencies for your custom classes.
- **init** This file will be run once your project is fully loaded (Titanium is loaded, custom classes loaded). It is the 'entry' point to your project where most of your application logic will likely reside.

You can set these properties using `tdt set pre-init <path>`, `tdt set titanium-init <path>`, and `tdt set init <path>`. In order for your project to load, you **must** have an 'init' file.

To unset any of these files, use the same command but provide no 'path' (eg: `tdt set pre-init` will unset the pre-init script).

##### Titanium
Assuming you will use Titanium for your application, you will need to have it loaded by the time your custom classes are loaded (ie: just after the 'titanium-init' script is executed). You can either load you own version of Titanium inside of 'titanium-init', or use TDT's built in Titanium version management.

Using TDT for Titanium installation management makes the whole process *much* easier, and completely removes the need for you to download/install Titanium.

The `tdt titanium` command accepts three types of arguments:
- None/nil (`tdt titanium`): Sets the application Titanium version to 'latest'
- False (`tdt titanium false`): Disables application Titanium management. You *must* manually load Titanium or else your project will refuse to load
- Specific version (`tdt titanium <version>`): Sets the application Titanium version to whatever you provide. The version is verified to ensure the version provided actually exists. Use `tpm fetch` to update the list of valid versions.

When the application has a version of Titanium set, the build file will automatically download, install and load that version of Titanium when your build file is executed. This means that the end-user doesn't have to even *think* about downloading Titanium, it's all done for them.

##### Building
Before building, use `tdt set output <path>` to dictate where your build file will be saved. Once set, use `tdt build` to build your project. All your settings will be passed to the Titanium packager, which will compile your project and save the build file to the computer.

Use `tdt run` to build your project and then immediately run the build file.

##### Minification
Use `tdt set minify true` to enable minification.

In order to minify your source code (files ending with `.ti` or `.lua`), the Titanium Packager will need access to a minifier. While building, the packager will:
- Look for a function at `_G.Minify`. If a function is found there it will be executed and the content of the file to be minified will be passed to it. The function is expected to return the minified version.
- If a function is NOT found in `_G.Minify`, the packager will execute the 'MINIFY_SOURCE' script. This script is expected to expose `_G.Minify`.

By default, 'MINIFY_SOURCE' is 'Minify.lua', this can be changed using `tdt set minify-source <path>`. Regardless of the path, it *must* expose `_G.Minify` in order for the packager to successfully minify the files.

##### Other Commands
###### view
`tdt view <mode>` allows you to view various properties of your project.  
- If 'mode' is 'settings', all configured settings are listed.
- If 'mode' is 'exclude' then all excluded paths are shown.
- If 'mode' is 'source' then source paths are listed.
- If 'mode' is 'projects', all registered projects are listed.

###### reset
`tdt reset` will reset your TDT configuration file -- This action is irreversible.

###### delete
Use `tdt delete <name>` to delete the project whose name matches the name provided.

##### Advanced Configuration
TDT allows you to manage finer configuration aspects of your project using the `tdt set` command. The following is a list of valid settings that haven't been covered already:

- **block-extract-override**: By default, an argument can be passed to your build file that dictates where the package will be extracted. Setting this option prevents this.
- **tianium-minify**: When downloading Titanium automatically, minified builds will be used (around half the size, although functionality is unaffected).
- **titanium-silent**: All output from TPM is hidden while downloading Titanium. Useful when using a custom loading screen.
- **titanium-disable-check**: Set this option to prevent TDT from warning you when no version of Titanium is selected (useful when loading your own version).
- **vfs-disable**: Disables the virtual file system, removing access to non-extract type source files (ie: files added via `tdt add` will be unavailable).
- **vfs-expose-global**: The raw environment of the package will be accessible (removes sandboxing). This means classes (including Titanium) is loaded into the global environment. Use of this setting is discouraged.
- **vfs-preserve-proxy**: If this package is run inside another package, the sandbox environment of the 'parent package' will be used instead of a second sandbox.
- **vfs-allow-raw-fs-access**: Allows access to the raw file system using `TI_VFS_RAW.fs`. Means that packages can perform `fs` operations without the virtual file system interfering.
- **vfs-allow-fs-redirection**: Exposes the `setVFSFallback` function, which can be used to the set the file system that the VFS will use as it's fallback (defaults to raw FS). Unless `true` is passed as the second argument, this function can only be used once (to prevent malicious use).
- **pickle-source**: When building outside of ComputerCraft, this setting must point to a file that, when executed, exposes a function in `_G.pickle`. This function will be called with a string which must then be serialised (same as `textutils.serialise` from ComputerCraft. Failure to serialise correctly will mean the build is unusable).

Other than `pickle-source` (requires an argument, `tdt set pickle-source <path>`), any of the above settings can be enabled, using `tdt set <settingName> true`. To unset, simply omit the 'true' (ie: use `tdt set <settingName>`).